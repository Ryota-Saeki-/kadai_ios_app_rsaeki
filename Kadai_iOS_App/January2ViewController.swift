import UIKit


class January2ViewController: UIViewController {
    
    @IBOutlet weak var textField1_1: UITextField!
    @IBOutlet weak var textField1_2: UITextField!
    @IBOutlet weak var textField1_3: UITextField!
    @IBOutlet weak var textField1_4: UITextField!
    
    var aText = Int()
    var a2Text = Int()
    var a3Text = Int()
    var a4Text = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField1_1.keyboardType = UIKeyboardType.numberPad
        textField1_2.keyboardType = UIKeyboardType.numberPad
        textField1_3.keyboardType = UIKeyboardType.numberPad
        textField1_4.keyboardType = UIKeyboardType.numberPad
    }
    
    
    
    @IBAction func okButton1(_ sender: Any) {
        if let num1 = textField1_1.text, let num1_2 = textField1_2.text , let num1_3 = textField1_3.text , let num1_4 = textField1_4.text{
        aText = Int(num1)!
        a2Text = Int(num1_2)!
        a3Text = Int(num1_3)!
        a4Text = Int(num1_4)!
        }else{
            
        }
//        performSegue(withIdentifier: "toBack1", sender: nil)
        dismiss(animated: true, completion: nil)
    }
    

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let nextVC = segue.destination as! JanuaryViewController

        // 引き継ぎたい変数たち
        nextVC.aText = aText
        nextVC.a2Text = a2Text
        nextVC.a3Text = a3Text
        nextVC.a4Text = a4Text
    
    }
}
