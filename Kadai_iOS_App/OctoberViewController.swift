//
//  OctoberViewController.swift
//  Kadai_iOS_App
//
//  Created by rsaeki on 2020/04/20.
//

import UIKit

class OctoberViewController: UIViewController {
    
    @IBOutlet weak var L10_1: UILabel!
    @IBOutlet weak var L10_2: UILabel!
    @IBOutlet weak var L10_3: UILabel!
    @IBOutlet weak var L10_4: UILabel!
    
    var a10_1Text = Int()
    var a10_2Text = Int()
    var a10_3Text = Int()
    var a10_4Text = Int()
    
    
    @IBOutlet weak var L10_5: UILabel!
    @IBOutlet weak var L10_6: UILabel!
    @IBOutlet weak var L10_7: UILabel!
    @IBOutlet weak var L10_8: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        L10_5.text = String(a10_1Text)
        L10_6.text = String(a10_2Text)
        L10_7.text = String(a10_3Text)
        L10_8.text = String(a10_4Text)
        
        L10_1.text = "趣味"
        L10_2.text = "食費"
        L10_3.text = "雑貨"
        L10_4.text = "その他"

        title = "10月"
    }
    

}
