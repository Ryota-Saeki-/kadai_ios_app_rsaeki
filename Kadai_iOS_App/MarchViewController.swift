//
//  MarchViewController.swift
//  Kadai_iOS_App
//
//  Created by rsaeki on 2020/04/20.
//

import UIKit

class MarchViewController: UIViewController {

    @IBOutlet weak var L3_1: UILabel!
    @IBOutlet weak var L3_2: UILabel!
    @IBOutlet weak var L3_3: UILabel!
    @IBOutlet weak var L3_4: UILabel!
    
    var a3_1Text = Int()
    var a3_2Text = Int()
    var a3_3Text = Int()
    var a3_4Text = Int()
    
    @IBOutlet weak var L3_5: UILabel!
    @IBOutlet weak var L3_6: UILabel!
    @IBOutlet weak var L3_7: UILabel!
    @IBOutlet weak var L3_8: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        L3_5.text = String(a3_1Text)
        L3_6.text = String(a3_2Text)
        L3_7.text = String(a3_3Text)
        L3_8.text = String(a3_4Text)
        
        L3_1.text = "趣味"
        L3_2.text = "食費"
        L3_3.text = "雑貨"
        L3_4.text = "その他"
        
        title = "3月"
    }
    
}
