import UIKit

class AugustViewController: UIViewController {
    
    @IBOutlet weak var L8_1: UILabel!
    @IBOutlet weak var L8_2: UILabel!
    @IBOutlet weak var L8_3: UILabel!
    @IBOutlet weak var L8_4: UILabel!
    
    var a8_1Text = Int()
    var a8_2Text = Int()
    var a8_3Text = Int()
    var a8_4Text = Int()
    
    @IBOutlet weak var L8_5: UILabel!
    @IBOutlet weak var L8_6: UILabel!
    @IBOutlet weak var L8_7: UILabel!
    @IBOutlet weak var L8_8: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        L8_5.text = String(a8_1Text)
        L8_6.text = String(a8_2Text)
        L8_7.text = String(a8_3Text)
        L8_8.text = String(a8_4Text)
        
        L8_1.text = "趣味"
        L8_2.text = "食費"
        L8_3.text = "雑貨"
        L8_4.text = "その他"

        title = "8月"
    }

}
