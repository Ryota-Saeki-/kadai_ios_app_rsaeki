import UIKit
import Charts

class JanuaryViewController: UIViewController {

    @IBOutlet weak var pieChartsView: PieChartView!
    
    @IBOutlet weak var L1_1: UILabel!
    @IBOutlet weak var L1_2: UILabel!
    @IBOutlet weak var L1_3: UILabel!
    @IBOutlet weak var L1_4: UILabel!
    
    var aText = Int()
    var a2Text = Int()
    var a3Text = Int()
    var a4Text = Int()
    
    
    @IBOutlet weak var L1_5: UILabel!
    @IBOutlet weak var L1_6: UILabel!
    @IBOutlet weak var L1_7: UILabel!
    @IBOutlet weak var L1_8: UILabel!
    
        override func viewDidLoad() {
        super.viewDidLoad()
            
            L1_5.text = String(aText)
            L1_6.text = String(a2Text)
            L1_7.text = String(a3Text)
            L1_8.text = String(a4Text)
            
            self.pieChartsView.centerText = "今月の割合"
            
            let dataEntries = [
                PieChartDataEntry(value: 40, label: "趣味"),
                PieChartDataEntry(value: 35, label: "食費"),
                PieChartDataEntry(value: 25, label: "雑貨"),
                PieChartDataEntry(value: 25, label: "その他")
            ]
            
            let dataSet = PieChartDataSet(entries: dataEntries, label: "今月の割合")

            // グラフの色
            dataSet.colors = ChartColorTemplates.vordiplom()
            // グラフのデータの値の色
            dataSet.valueTextColor = UIColor.black
            // グラフのデータのタイトルの色
            dataSet.entryLabelColor = UIColor.black

            self.pieChartsView.data = PieChartData(dataSet: dataSet)
            
            // データを％表示にする
            let formatter = NumberFormatter()
            formatter.numberStyle = .percent
            formatter.maximumFractionDigits = 2
            formatter.multiplier = 1.0
            self.pieChartsView.data?.setValueFormatter(DefaultValueFormatter(formatter: formatter))
            self.pieChartsView.usePercentValuesEnabled = true
            
            view.addSubview(self.pieChartsView)
        
        
        L1_1.text = "趣味"
        L1_2.text = "食費"
        L1_3.text = "雑貨"
        L1_4.text = "その他"

        title = "1月"
    }
}
