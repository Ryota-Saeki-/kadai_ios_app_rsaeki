import UIKit

class FebruaryViewController2: UIViewController {
    
    
    @IBOutlet weak var textField2_1: UITextField!
    @IBOutlet weak var textField2_2: UITextField!
    @IBOutlet weak var textField2_3: UITextField!
    @IBOutlet weak var textField2_4: UITextField!
    
    var a2_1Text = Int()
    var a2_2Text = Int()
    var a2_3Text = Int()
    var a2_4Text = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField2_1.keyboardType = UIKeyboardType.numberPad
        self.textField2_2.keyboardType = UIKeyboardType.numberPad
        self.textField2_3.keyboardType = UIKeyboardType.numberPad
        self.textField2_4.keyboardType = UIKeyboardType.numberPad
        
    }
    
    @IBAction func okButton2(_ sender: Any) {
        if let num2_1 = textField2_1.text, let num2_2 = textField2_2.text , let num2_3 = textField2_3.text , let num2_4 = textField2_4.text{
                a2_1Text = Int(num2_1)!
                a2_2Text = Int(num2_2)!
                a2_3Text = Int(num2_3)!
                a2_4Text = Int(num2_4)!
                }else{
                    
                }
        //        performSegue(withIdentifier: "toBack1", sender: nil)
                dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let nextVC = segue.destination as! FebruaryViewController

        // 引き継ぎたい変数たち
        nextVC.a2_1Text = a2_1Text
        nextVC.a2_2Text = a2_2Text
        nextVC.a2_3Text = a2_3Text
        nextVC.a2_4Text = a2_4Text
    
    }
    

}
