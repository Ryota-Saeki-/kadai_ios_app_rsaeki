//
//  NovemViewController2.swift
//  Kadai_iOS_App
//
//  Created by rsaeki on 2020/05/11.
//

import UIKit

class NovemViewController2: UIViewController {

    @IBOutlet weak var textField11_1: UITextField!
    @IBOutlet weak var textField11_2: UITextField!
    @IBOutlet weak var textField11_3: UITextField!
    @IBOutlet weak var textField11_4: UITextField!
    
    var a11_1Text = Int()
    var a11_2Text = Int()
    var a11_3Text = Int()
    var a11_4Text = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField11_1.keyboardType = UIKeyboardType.numberPad
        self.textField11_2.keyboardType = UIKeyboardType.numberPad
        self.textField11_3.keyboardType = UIKeyboardType.numberPad
        self.textField11_4.keyboardType = UIKeyboardType.numberPad
    }
    
    
    @IBAction func okButton11(_ sender: Any) {
        if let num11_1 = textField11_1.text, let num11_2 = textField11_2.text , let num11_3 = textField11_3.text , let num11_4 = textField11_4.text{
                a11_1Text = Int(num11_1)!
                a11_2Text = Int(num11_2)!
                a11_3Text = Int(num11_3)!
                a11_4Text = Int(num11_4)!
                }else{
                    
                }
        //        performSegue(withIdentifier: "toBack1", sender: nil)
                dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let nextVC = segue.destination as! NovemberViewController

        // 引き継ぎたい変数たち
        nextVC.a11_1Text = a11_1Text
        nextVC.a11_2Text = a11_2Text
        nextVC.a11_3Text = a11_3Text
        nextVC.a11_4Text = a11_4Text
    
    }

}
