import UIKit

class MarchViewController2: UIViewController {

    @IBOutlet weak var textField3_1: UITextField!
    @IBOutlet weak var textField3_2: UITextField!
    @IBOutlet weak var textField3_3: UITextField!
    @IBOutlet weak var textField3_4: UITextField!
    
    var a3_1Text = Int()
    var a3_2Text = Int()
    var a3_3Text = Int()
    var a3_4Text = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textField3_1.keyboardType = UIKeyboardType.numberPad
        self.textField3_2.keyboardType = UIKeyboardType.numberPad
        self.textField3_3.keyboardType = UIKeyboardType.numberPad
        self.textField3_4.keyboardType = UIKeyboardType.numberPad

    }
    
    @IBAction func okButton3(_ sender: Any) {
        if let num3_1 = textField3_1.text, let num3_2 = textField3_2.text , let num3_3 = textField3_3.text , let num3_4 = textField3_4.text{
                a3_1Text = Int(num3_1)!
                a3_2Text = Int(num3_2)!
                a3_3Text = Int(num3_3)!
                a3_4Text = Int(num3_4)!
                }else{
                    
                }
        //        performSegue(withIdentifier: "toBack1", sender: nil)
                dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let nextVC = segue.destination as! MarchViewController

        // 引き継ぎたい変数たち
        nextVC.a3_1Text = a3_1Text
        nextVC.a3_2Text = a3_2Text
        nextVC.a3_3Text = a3_3Text
        nextVC.a3_4Text = a3_4Text
    
    }
    
}
