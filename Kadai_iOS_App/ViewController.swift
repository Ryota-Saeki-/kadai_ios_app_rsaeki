//
//  ViewController.swift
//  Kadai_iOS_App
//
//  Created by rsaeki on 2020/04/16.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let sectionData = ["合計", "月選択"]
       // ↓修正 <テーブル表示データ>
    let tableData = [["年間合計"],["節約しよーね(^__^)","1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"]]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Top"
        
    }
    
    // セクション毎の行数を返す
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // ↓修正
        return tableData[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionData.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 40
    }
    
    // 各行に表示するセルを返す
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // StoryBoradで定義したTableViewCellを取得する
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = tableData[indexPath.section][indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionData[section]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.section == 0){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)

                   // 別の画面に遷移
                   performSegue(withIdentifier: "toYear", sender: nil)
        }
        else if(indexPath.row == 1){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)

                   // 別の画面に遷移
                   performSegue(withIdentifier: "toJanuary", sender: nil)
        }
        else if(indexPath.row == 2){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)

                   // 別の画面に遷移
                   performSegue(withIdentifier: "toFebruary", sender: nil)
        }
        else if(indexPath.row == 3){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)

                   // 別の画面に遷移
                   performSegue(withIdentifier: "toMarch", sender: nil)
        }
        else if(indexPath.row == 4){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)

                   // 別の画面に遷移
                   performSegue(withIdentifier: "toApril", sender: nil)
        }
        else if(indexPath.row == 5){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)

                   // 別の画面に遷移
                   performSegue(withIdentifier: "toMay", sender: nil)
        }
        else if(indexPath.row == 6){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)

                   // 別の画面に遷移
                   performSegue(withIdentifier: "toJune", sender: nil)
        }
        else if(indexPath.row == 7){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)

                   // 別の画面に遷移
                   performSegue(withIdentifier: "toJuly", sender: nil)
        }
        else if(indexPath.row == 8){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)

                   // 別の画面に遷移
                   performSegue(withIdentifier: "toAugust", sender: nil)
        }
        else if(indexPath.row == 9){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)

                   // 別の画面に遷移
                   performSegue(withIdentifier: "toSeptember", sender: nil)
        }
        else if(indexPath.row == 10){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)

                   // 別の画面に遷移
                   performSegue(withIdentifier: "toOctober", sender: nil)
        }
        else if(indexPath.row == 11){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)

                   // 別の画面に遷移
                   performSegue(withIdentifier: "toNovember", sender: nil)
        }
        else if(indexPath.row == 12){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)

                   // 別の画面に遷移
                   performSegue(withIdentifier: "toDecember", sender: nil)
        }

    }
}
