//
//  FebruaryViewController.swift
//  Kadai_iOS_App
//
//  Created by rsaeki on 2020/04/20.
//

import UIKit

class FebruaryViewController: UIViewController {

    @IBOutlet weak var L2_1: UILabel!
    @IBOutlet weak var L2_2: UILabel!
    @IBOutlet weak var L2_3: UILabel!
    @IBOutlet weak var L2_4: UILabel!
    
    var a2_1Text = Int()
    var a2_2Text = Int()
    var a2_3Text = Int()
    var a2_4Text = Int()
    
    @IBOutlet weak var L2_5: UILabel!
    @IBOutlet weak var L2_6: UILabel!
    @IBOutlet weak var L2_7: UILabel!
    @IBOutlet weak var L2_8: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        L2_5.text = String(a2_1Text)
        L2_6.text = String(a2_2Text)
        L2_7.text = String(a2_3Text)
        L2_8.text = String(a2_4Text)
        
        L2_1.text = "趣味"
        L2_2.text = "食費"
        L2_3.text = "雑貨"
        L2_4.text = "その他"
        
        title = "2月"
    }

}
