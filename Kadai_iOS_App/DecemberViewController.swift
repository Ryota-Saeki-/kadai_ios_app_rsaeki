import UIKit

class DecemberViewController: UIViewController {
    
    
    @IBOutlet weak var L12_1: UILabel!
    @IBOutlet weak var L12_2: UILabel!
    @IBOutlet weak var L12_3: UILabel!
    @IBOutlet weak var L12_4: UILabel!
    
    var a12_1Text = Int()
    var a12_2Text = Int()
    var a12_3Text = Int()
    var a12_4Text = Int()
    
    
    @IBOutlet weak var L12_5: UILabel!
    @IBOutlet weak var L12_6: UILabel!
    @IBOutlet weak var L12_7: UILabel!
    @IBOutlet weak var L12_8: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        L12_5.text = String(a12_1Text)
        L12_6.text = String(a12_2Text)
        L12_7.text = String(a12_3Text)
        L12_8.text = String(a12_4Text)
        
        L12_1.text = "趣味"
        L12_2.text = "食費"
        L12_3.text = "雑貨"
        L12_4.text = "その他"
        
        title = "12月"
        
    }
    

}
