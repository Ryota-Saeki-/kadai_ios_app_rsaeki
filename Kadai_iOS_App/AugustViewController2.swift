import UIKit

class AugustViewController2: UIViewController {
    
    @IBOutlet weak var textField8_1: UITextField!
    @IBOutlet weak var textField8_2: UITextField!
    @IBOutlet weak var textField8_3: UITextField!
    @IBOutlet weak var textField8_4: UITextField!
    
    var a8_1Text = Int()
    var a8_2Text = Int()
    var a8_3Text = Int()
    var a8_4Text = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField8_1.keyboardType = UIKeyboardType.numberPad
        self.textField8_2.keyboardType = UIKeyboardType.numberPad
        self.textField8_3.keyboardType = UIKeyboardType.numberPad
        self.textField8_4.keyboardType = UIKeyboardType.numberPad

    }
    
    @IBAction func okButton8(_ sender: Any) {
        if let num8_1 = textField8_1.text, let num8_2 = textField8_2.text , let num8_3 = textField8_3.text , let num8_4 = textField8_4.text{
                a8_1Text = Int(num8_1)!
                a8_2Text = Int(num8_2)!
                a8_3Text = Int(num8_3)!
                a8_4Text = Int(num8_4)!
                }else{
                    
                }
        //        performSegue(withIdentifier: "toBack1", sender: nil)
                dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let nextVC = segue.destination as! AugustViewController

        // 引き継ぎたい変数たち
        nextVC.a8_1Text = a8_1Text
        nextVC.a8_2Text = a8_2Text
        nextVC.a8_3Text = a8_3Text
        nextVC.a8_4Text = a8_4Text
    
    }
    
}
