import UIKit

class DecemberViewController2: UIViewController {
    
    @IBOutlet weak var textField12_1: UITextField!
    @IBOutlet weak var textField12_2: UITextField!
    @IBOutlet weak var textField12_3: UITextField!
    @IBOutlet weak var textField12_4: UITextField!
    
    var a12_1Text = Int()
    var a12_2Text = Int()
    var a12_3Text = Int()
    var a12_4Text = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField12_1.keyboardType = UIKeyboardType.numberPad
        self.textField12_2.keyboardType = UIKeyboardType.numberPad
        self.textField12_3.keyboardType = UIKeyboardType.numberPad
        self.textField12_4.keyboardType = UIKeyboardType.numberPad
    }
    @IBAction func okButton12(_ sender: Any) {
        if let num12_1 = textField12_1.text, let num12_2 = textField12_2.text , let num12_3 = textField12_3.text , let num12_4 = textField12_4.text{
                a12_1Text = Int(num12_1)!
                a12_2Text = Int(num12_2)!
                a12_3Text = Int(num12_3)!
                a12_4Text = Int(num12_4)!
                }else{
                    
                }
        //        performSegue(withIdentifier: "toBack1", sender: nil)
                dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let nextVC = segue.destination as! DecemberViewController

        // 引き継ぎたい変数たち
        nextVC.a12_1Text = a12_1Text
        nextVC.a12_2Text = a12_2Text
        nextVC.a12_3Text = a12_3Text
        nextVC.a12_4Text = a12_4Text
    
    }
    
    
}
