
import UIKit

class OctoberViewController2: UIViewController {
    
    @IBOutlet weak var textField10_1: UITextField!
    @IBOutlet weak var textField10_2: UITextField!
    @IBOutlet weak var textField10_3: UITextField!
    @IBOutlet weak var textField10_4: UITextField!
    
    var a10_1Text = Int()
    var a10_2Text = Int()
    var a10_3Text = Int()
    var a10_4Text = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField10_1.keyboardType = UIKeyboardType.numberPad
        self.textField10_2.keyboardType = UIKeyboardType.numberPad
        self.textField10_3.keyboardType = UIKeyboardType.numberPad
        self.textField10_4.keyboardType = UIKeyboardType.numberPad
    }
    
    @IBAction func okButton10(_ sender: Any) {
        if let num10_1 = textField10_1.text, let num10_2 = textField10_2.text , let num10_3 = textField10_3.text , let num10_4 = textField10_4.text{
                a10_1Text = Int(num10_1)!
                a10_2Text = Int(num10_2)!
                a10_3Text = Int(num10_3)!
                a10_4Text = Int(num10_4)!
                }else{
                    
                }
        //        performSegue(withIdentifier: "toBack1", sender: nil)
                dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let nextVC = segue.destination as! OctoberViewController

        // 引き継ぎたい変数たち
        nextVC.a10_1Text = a10_1Text
        nextVC.a10_2Text = a10_2Text
        nextVC.a10_3Text = a10_3Text
        nextVC.a10_4Text = a10_4Text
    
    }

   

}
