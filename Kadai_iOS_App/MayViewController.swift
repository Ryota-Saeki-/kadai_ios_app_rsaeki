import UIKit

class MayViewController: UIViewController {

    @IBOutlet weak var L5_1: UILabel!
    @IBOutlet weak var L5_2: UILabel!
    @IBOutlet weak var L5_3: UILabel!
    @IBOutlet weak var L5_4: UILabel!
    
    var a5_1Text = Int()
    var a5_2Text = Int()
    var a5_3Text = Int()
    var a5_4Text = Int()
    
    @IBOutlet weak var L5_5: UILabel!
    @IBOutlet weak var L5_6: UILabel!
    @IBOutlet weak var L5_7: UILabel!
    @IBOutlet weak var L5_8: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        L5_5.text = String(a5_1Text)
        L5_6.text = String(a5_2Text)
        L5_7.text = String(a5_3Text)
        L5_8.text = String(a5_4Text)
        
        L5_1.text = "趣味"
        L5_2.text = "食費"
        L5_3.text = "雑貨"
        L5_4.text = "その他"
        
        title = "5月"
    }
}
