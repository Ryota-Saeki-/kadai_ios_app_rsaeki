import UIKit

class JulyViewController: UIViewController {

    @IBOutlet weak var L7_1: UILabel!
    @IBOutlet weak var L7_2: UILabel!
    @IBOutlet weak var L7_3: UILabel!
    @IBOutlet weak var L7_4: UILabel!
    
    var a7_1Text = Int()
    var a7_2Text = Int()
    var a7_3Text = Int()
    var a7_4Text = Int()
    
    @IBOutlet weak var L7_5: UILabel!
    @IBOutlet weak var L7_6: UILabel!
    @IBOutlet weak var L7_7: UILabel!
    @IBOutlet weak var L7_8: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        L7_5.text = String(a7_1Text)
        L7_6.text = String(a7_2Text)
        L7_7.text = String(a7_3Text)
        L7_8.text = String(a7_4Text)
        
        L7_1.text = "趣味"
        L7_2.text = "食費"
        L7_3.text = "雑貨"
        L7_4.text = "その他"

        title = "7月"
    }


}
