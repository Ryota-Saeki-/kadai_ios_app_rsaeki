import UIKit

class JulyViewController2: UIViewController {
    
    @IBOutlet weak var textField7_1: UITextField!
    @IBOutlet weak var textField7_2: UITextField!
    @IBOutlet weak var textField7_3: UITextField!
    @IBOutlet weak var textField7_4: UITextField!
    
    var a7_1Text = Int()
    var a7_2Text = Int()
    var a7_3Text = Int()
    var a7_4Text = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textField7_1.keyboardType = UIKeyboardType.numberPad
        self.textField7_2.keyboardType = UIKeyboardType.numberPad
        self.textField7_3.keyboardType = UIKeyboardType.numberPad
        self.textField7_4.keyboardType = UIKeyboardType.numberPad

    }
    
    @IBAction func okButton7(_ sender: Any) {
        if let num7_1 = textField7_1.text, let num7_2 = textField7_2.text , let num7_3 = textField7_3.text , let num7_4 = textField7_4.text{
                a7_1Text = Int(num7_1)!
                a7_2Text = Int(num7_2)!
                a7_3Text = Int(num7_3)!
                a7_4Text = Int(num7_4)!
                }else{
                    
                }
        //        performSegue(withIdentifier: "toBack1", sender: nil)
                dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

           let nextVC = segue.destination as! JulyViewController

           // 引き継ぎたい変数たち
           nextVC.a7_1Text = a7_1Text
           nextVC.a7_2Text = a7_2Text
           nextVC.a7_3Text = a7_3Text
           nextVC.a7_4Text = a7_4Text
       
       }
    

}
