//
//  SeptemberViewController.swift
//  Kadai_iOS_App
//
//  Created by rsaeki on 2020/04/20.
//

import UIKit

class SeptemberViewController: UIViewController {

    @IBOutlet weak var L9_1: UILabel!
    @IBOutlet weak var L9_2: UILabel!
    @IBOutlet weak var L9_3: UILabel!
    @IBOutlet weak var L9_4: UILabel!
    
    var a9_1Text = Int()
    var a9_2Text = Int()
    var a9_3Text = Int()
    var a9_4Text = Int()
    
    
    @IBOutlet weak var L9_5: UILabel!
    @IBOutlet weak var L9_6: UILabel!
    @IBOutlet weak var L9_7: UILabel!
    @IBOutlet weak var L9_8: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        L9_5.text = String(a9_1Text)
        L9_6.text = String(a9_2Text)
        L9_7.text = String(a9_3Text)
        L9_8.text = String(a9_4Text)
        
        L9_1.text = "趣味"
        L9_2.text = "食費"
        L9_3.text = "雑貨"
        L9_4.text = "その他"

        title = "9月"
    }

}
