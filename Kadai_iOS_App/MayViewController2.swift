import UIKit

class MayViewController2: UIViewController {
    
    
    @IBOutlet weak var textField5_1: UITextField!
    @IBOutlet weak var textField5_2: UITextField!
    @IBOutlet weak var textField5_3: UITextField!
    @IBOutlet weak var textField5_4: UITextField!
    
    var a5_1Text = Int()
    var a5_2Text = Int()
    var a5_3Text = Int()
    var a5_4Text = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField5_1.keyboardType = UIKeyboardType.numberPad
        self.textField5_2.keyboardType = UIKeyboardType.numberPad
        self.textField5_3.keyboardType = UIKeyboardType.numberPad
        self.textField5_4.keyboardType = UIKeyboardType.numberPad
    }
    
    @IBAction func okButton5(_ sender: Any) {
        if let num5_1 = textField5_1.text, let num5_2 = textField5_2.text , let num5_3 = textField5_3.text , let num5_4 = textField5_4.text{
                a5_1Text = Int(num5_1)!
                a5_2Text = Int(num5_2)!
                a5_3Text = Int(num5_3)!
                a5_4Text = Int(num5_4)!
                }else{
                    
                }
        //        performSegue(withIdentifier: "toBack1", sender: nil)
                dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let nextVC = segue.destination as! MayViewController

        // 引き継ぎたい変数たち
        nextVC.a5_1Text = a5_1Text
        nextVC.a5_2Text = a5_2Text
        nextVC.a5_3Text = a5_3Text
        nextVC.a5_4Text = a5_4Text
    
    }
}
