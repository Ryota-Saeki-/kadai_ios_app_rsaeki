import UIKit

class JuneViewController: UIViewController {
    
    @IBOutlet weak var L6_1: UILabel!
    @IBOutlet weak var L6_2: UILabel!
    @IBOutlet weak var L6_3: UILabel!
    @IBOutlet weak var L6_4: UILabel!
    
    var a6_1Text = Int()
    var a6_2Text = Int()
    var a6_3Text = Int()
    var a6_4Text = Int()
    
    
    @IBOutlet weak var L6_5: UILabel!
    @IBOutlet weak var L6_6: UILabel!
    @IBOutlet weak var L6_7: UILabel!
    @IBOutlet weak var L6_8: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        L6_5.text = String(a6_1Text)
        L6_6.text = String(a6_2Text)
        L6_7.text = String(a6_3Text)
        L6_8.text = String(a6_4Text)
        
        
        L6_1.text = "趣味"
        L6_2.text = "食費"
        L6_3.text = "雑貨"
        L6_4.text = "その他"

       title = "6月"
    }
    
}
