import UIKit

class AprilViewController: UIViewController {

    @IBOutlet weak var L4_1: UILabel!
    @IBOutlet weak var L4_2: UILabel!
    @IBOutlet weak var L4_3: UILabel!
    @IBOutlet weak var L4_4: UILabel!
    
    var a4_1Text = Int()
    var a4_2Text = Int()
    var a4_3Text = Int()
    var a4_4Text = Int()
    
    @IBOutlet weak var L4_5: UILabel!
    @IBOutlet weak var L4_6: UILabel!
    @IBOutlet weak var L4_7: UILabel!
    @IBOutlet weak var L4_8: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        L4_5.text = String(a4_1Text)
        L4_6.text = String(a4_2Text)
        L4_7.text = String(a4_3Text)
        L4_8.text = String(a4_4Text)
        
        L4_1.text = "趣味"
        L4_2.text = "食費"
        L4_3.text = "雑貨"
        L4_4.text = "その他"
    
        title = "4月"
    }

}
