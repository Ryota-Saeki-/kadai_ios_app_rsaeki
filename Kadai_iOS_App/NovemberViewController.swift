//
//  NovemberViewController.swift
//  Kadai_iOS_App
//
//  Created by rsaeki on 2020/04/20.
//

import UIKit

class NovemberViewController: UIViewController {

    @IBOutlet weak var L11_1: UILabel!
    @IBOutlet weak var L11_2: UILabel!
    @IBOutlet weak var L11_3: UILabel!
    @IBOutlet weak var L11_4: UILabel!
    
    var a11_1Text = Int()
    var a11_2Text = Int()
    var a11_3Text = Int()
    var a11_4Text = Int()
    
    @IBOutlet weak var L11_5: UILabel!
    @IBOutlet weak var L11_6: UILabel!
    @IBOutlet weak var L11_7: UILabel!
    @IBOutlet weak var L11_8: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        L11_5.text = String(a11_1Text)
        L11_6.text = String(a11_2Text)
        L11_7.text = String(a11_3Text)
        L11_8.text = String(a11_4Text)
        
        L11_1.text = "趣味"
        L11_2.text = "食費"
        L11_3.text = "雑貨"
        L11_4.text = "その他"

        title = "11月"
    }
    


}
