import UIKit

class SeptemberViewController2: UIViewController {
    
    @IBOutlet weak var textField9_1: UITextField!
    @IBOutlet weak var textField9_2: UITextField!
    @IBOutlet weak var textField9_3: UITextField!
    @IBOutlet weak var textField9_4: UITextField!
    
    var a9_1Text = Int()
    var a9_2Text = Int()
    var a9_3Text = Int()
    var a9_4Text = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField9_1.keyboardType = UIKeyboardType.numberPad
        self.textField9_2.keyboardType = UIKeyboardType.numberPad
        self.textField9_3.keyboardType = UIKeyboardType.numberPad
        self.textField9_4.keyboardType = UIKeyboardType.numberPad

        
    }
    
    @IBAction func okButton9(_ sender: Any) {
        if let num9_1 = textField9_1.text, let num9_2 = textField9_2.text , let num9_3 = textField9_3.text , let num9_4 = textField9_4.text{
                a9_1Text = Int(num9_1)!
                a9_2Text = Int(num9_2)!
                a9_3Text = Int(num9_3)!
                a9_4Text = Int(num9_4)!
                }else{
                    
                }
        //        performSegue(withIdentifier: "toBack1", sender: nil)
                dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let nextVC = segue.destination as! SeptemberViewController

        // 引き継ぎたい変数たち
        nextVC.a9_1Text = a9_1Text
        nextVC.a9_2Text = a9_2Text
        nextVC.a9_3Text = a9_3Text
        nextVC.a9_4Text = a9_4Text
    
    }
    

}
