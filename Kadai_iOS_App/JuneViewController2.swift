import UIKit

class JuneViewController2: UIViewController {
    
    
    @IBOutlet weak var textField6_1: UITextField!
    @IBOutlet weak var textField6_2: UITextField!
    @IBOutlet weak var textField6_3: UITextField!
    @IBOutlet weak var textField6_4: UITextField!
    
    var a6_1Text = Int()
    var a6_2Text = Int()
    var a6_3Text = Int()
    var a6_4Text = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField6_1.keyboardType = UIKeyboardType.numberPad
        self.textField6_2.keyboardType = UIKeyboardType.numberPad
        self.textField6_3.keyboardType = UIKeyboardType.numberPad
        self.textField6_4.keyboardType = UIKeyboardType.numberPad
    }
    @IBAction func okButton6(_ sender: Any) {
        if let num6_1 = textField6_1.text, let num6_2 = textField6_2.text , let num6_3 = textField6_3.text , let num6_4 = textField6_4.text{
                a6_1Text = Int(num6_1)!
                a6_2Text = Int(num6_2)!
                a6_3Text = Int(num6_3)!
                a6_4Text = Int(num6_4)!
                }else{
                    
                }
        //        performSegue(withIdentifier: "toBack1", sender: nil)
                dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let nextVC = segue.destination as! JuneViewController

        // 引き継ぎたい変数たち
        nextVC.a6_1Text = a6_1Text
        nextVC.a6_2Text = a6_2Text
        nextVC.a6_3Text = a6_3Text
        nextVC.a6_4Text = a6_4Text
    
    }
    

}
