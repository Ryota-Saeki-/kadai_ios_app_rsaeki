import UIKit

class AprilViewController2: UIViewController {
    
    @IBOutlet weak var textField4_1: UITextField!
    @IBOutlet weak var textField4_2: UITextField!
    @IBOutlet weak var textField4_3: UITextField!
    @IBOutlet weak var textField4_4: UITextField!
    
    var a4_1Text = Int()
    var a4_2Text = Int()
    var a4_3Text = Int()
    var a4_4Text = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField4_1.keyboardType = UIKeyboardType.numberPad
        self.textField4_2.keyboardType = UIKeyboardType.numberPad
        self.textField4_3.keyboardType = UIKeyboardType.numberPad
        self.textField4_4.keyboardType = UIKeyboardType.numberPad
        
    }

    @IBAction func okButton4(_ sender: Any) {
        if let num4_1 = textField4_1.text, let num4_2 = textField4_2.text , let num4_3 = textField4_3.text , let num4_4 = textField4_4.text{
                a4_1Text = Int(num4_1)!
                a4_2Text = Int(num4_2)!
                a4_3Text = Int(num4_3)!
                a4_4Text = Int(num4_4)!
                }else{
                    
                }
        //        performSegue(withIdentifier: "toBack1", sender: nil)
                dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let nextVC = segue.destination as! AprilViewController

        // 引き継ぎたい変数たち
        nextVC.a4_1Text = a4_1Text
        nextVC.a4_2Text = a4_2Text
        nextVC.a4_3Text = a4_3Text
        nextVC.a4_4Text = a4_4Text
    
    }
}
